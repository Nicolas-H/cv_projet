<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CV Nicolas Hermosilla</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="assets/style/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <!-- /CSS -->

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css"
        integrity="sha384-Mmxa0mLqhmOeaE8vgOSbKacftZcsNYDjQzuCOm6D02luYSzBG8vpaOykv9lFQ51Y" crossorigin="anonymous">
    <!-- /Font -->

    <!-- meta SEO -->
    <meta name="description" content="mettre mot clés">
    <meta name="keywords" content="">
    <!-- /meta SEO -->

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
</head>


<!-- body -->

<body>

    <!-- header -->

    <header class="header">
        <a href="#" class="header-logo"><i class="fas fa-signal"></i>NICOLAS HERMOSILLA</a>
        <nav class="close">
            <a href="#1"><span>Accueil</span></a>
            <a href="#2"><span>Compétences</span></a>
            <a href="#3"><span>Parcours</span></a>
            <a href="#4"><span>Projets</span></a>
            <a href="#5"><span>Contact</span></a>
        </nav>
        <a class="toggle" id="burgerButton">
            <i class="fas fa-bars"></i>
        </a>
    </header>

    <!-- / header -->

    <!-- banner -->
    <section class="home">
        <div class="banner" data-speed="1">

            <div class="banner-content">
                <h1 class="title is-1">Bienvenue sur mon cv</h1>
                <h2 class="subtitle">Découvrez mes travaux et mon univers !</h2>
                <button class="button is-link"> Contactez-moi !</button> 
            </div>
        </div>
    </section>

    <!-- / banner -->

    <!-- Profil -->
    <section class="profil" id="1">
        <div class="section-title">
            <h1 id="profile-title" class="title is-1">- MON <span id="profile-title2" class="title is-1">PROFIL</span> -
            </h1>
        </div>
        <div class="row">

            <img src="/assets/img/profile.jpg" alt="profile picture">

            <div class="description">
                <span>
                    En reconversion professionnelle et ayant travaillé 2 ans en tant qu'apprenti commercial, j'ai repris
                    mes études en informatique par le biais de la formation INGESUP proposée par YNOV CAMPUS de
                    Toulouse.
                    Je souhaiterais m'orienter en réseau et sécurité informatique à l'issue de cette formation.
                </span>
                <div class="download_cv">
                    <i class="fas fa-cloud-download-alt"></i>
                    <a href="assets/Hermosilla_CV.pdf" download>télécharger mon cv</a>
                </div>

            </div>
            <div class="personnal-informations">
                <span>
                    <i class="fas fa-calendar-alt"></i><b>Naissance</b> : 1993<br>
                    <i class="fas fa-mobile-alt"></i><b>Téléphone</b> : 06 61 56 38 83<br>
                    <i class="far fa-envelope"></i><b>Mail</b> : nicolas.hermosilla92@gmail.com<br>
                    <i class="fas fa-home"></i><b>Adresse</b> : Toulouse


                </span>

            </div>



        </div>

    </section>
    <!-- /Profil -->
    <!-- Background -->
    <div class="background" data-speed="-5">
        <div class="filter">

        </div>

    </div>
    <!-- /Background -->
    <!-- Compétences -->


    <section class="skills" id="2">

        <div class="section-title">
            <h1 id="profile-title" class="title is-1">- MES <span id="profile-title2"
                    class="title is-1">COMPÉTENCES</span> -</h1>
        </div>

        <div id="skills">
            <div class="container">
                <div class="language">
                    <h2 class="text-center">Programmation</h2>

                    <div class="charts">
                        <div class=python>
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-python">
                                    <canvas id="python" height="150" width="150"></canvas>
                                </div>
                                <p>Python</p>
                                <br>
                            </div>
                        </div>


                        <div class=html5>
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-html">
                                    <canvas id="html" height="150" width="150"></canvas>
                                </div>
                                <p>HTML5</p>
                                <br>
                            </div>
                        </div>

                        <div class=css3>
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-css">
                                    <canvas id="css" height="150" width="150"></canvas>
                                </div>
                                <p>CSS3</p>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

                <h2 class="text-center">Systèmes et Réseau</h2>
                <div class="system-network">
                    <div class="charts">
                        <div class="windows-server">
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-ws">
                                    <canvas id="ws" height="150" width="150"></canvas>
                                </div>
                                <p>Windows Server</p>
                                <br>
                            </div>
                        </div>

                        <div class="virtual-box">
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-vb">
                                    <canvas id="vb" height="150" width="150"></canvas>
                                </div>
                                <p>Virtual Box</p>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

                <h2 class="text-center">Système d'exploitation</h2>
                <div class="OS">
                    <div class="charts">

                        <div class="windows">
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-windows">
                                    <canvas id="windows" height="150" width="150"></canvas>
                                </div>
                                <p>Windows</p>
                                <br>
                            </div>
                        </div>

                        <div class="mac">
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-mac">
                                    <canvas id="mac" height="150" width="150"></canvas>
                                </div>
                                <p>Mac OSX</p>
                                <br>
                            </div>
                        </div>

                        <div class="linux">
                            <div class="col-xs-6 col-sm-3 col-sm-offset-1">
                                <div id="canvas-wrapper-linux">
                                    <canvas id="linux" height="150" width="150"></canvas>
                                </div>
                                <p>Linux</p>
                                <br>
                            </div>
                        </div>

                    </div>
                </div>
    </section>


    <!-- /Compétences -->

    <div class="background2" data-speed="35">
        <div class="filter">

        </div>

    </div>

    <!-- Parcours -->

    <section class="course" id="3">

        <div class="section-title">
            <h1 id="profile-title" class="title is-1">- MON <span id="profile-title2" class="title is-1">PARCOURS</span>
                -</h1>
        </div>

        <div id="experiences">


            <div class="job">
                <span class="job-title">VENDEUR</span><br>
                <span class="date-detail">Octobre 2018 - Juin 2018, Marché de Colomiers</span>
            </div>


            <div class="job">
                <span class="job-title">SERVEUR</span><br>
                <span class="date-detail">Octobre 2017 - Mars 2018, Wisdom (Australie)</span>
            </div>

            <div class="job">
                <span class="job-title">TRAVAILLEUR AGRICOLE</span><br>
                <span class="date-detail">Janvier 2017 - Mai 2017, Flavorite Tomatoes (Australie) </span>
            </div>

            <div class="job">
                <span class="job-title">SERVEUR</span><br>
                <span class="date-detail">Octobre 2016 - Décembre 2016, Bar & Grill (Australie) </span>
            </div>

            <div class="job">
                <span class="job-title">EMPLOYÉ DE VENTE</span><br>
                <span class="date-detail">Septembre 2016, Grand Frais</span>
                <div class="job-detail">
                    <span>- Réceptionner et contrôler les livraisons<br>
                        - Mettre en rayon<br>
                        - Servir, renseigner la clientèle<br><br>
                    </span>
                </div>
            </div>

            <div class="job">
                <span class="job-title">ATTACHÉ COMMERCIAL</span><br>
                <span class="date-detail">Septembre 2014 - Aout 2016, Orexad</span>
                <div class="job-detail">
                    <span>- Gestion de clientèle B to B<br>
                        - Prospection téléphonique<br>
                        - Analyse et définition des besoins<br>
                        - Proposition de solutions techniques et d'offres commerciales<br>
                        - Gestion de suivi et fidélisation dex clients<br>
                        - Logistique (Réception et envoi de colis)<br>
                    </span>
                </div>
            </div>
        </div>
    </section>

    <!-- /Parcours -->

    <div class="background3" data-speed="35">
        <div class="filter">

        </div>
    </div>

    <!--- Formation -->

    <section class="education" id="4">

        <div class="section-title">
            <h1 id="profile-title" class="title is-1">- MES <span id="profile-title2"
                    class="title is-1">FORMATIONS</span> -</h1>
        </div>
        <div id="training-qualifications">

            <div class="training">
                <span class="training-title"> Bachelor Ingesup, Architecture Informatique et Systèmes
                    d'Information</span>
                <span class="training-location">- Campus Ynov Toulouse, 31</span>
                <span class="training-date"><br>2018 - 2020</span>
            </div>
            <div class="training">
                <span class="training-title"> UFO English</span>
                <span class="training-location">- English Language College (Sydney, Australie)</span>
                <span class="training-date"><br>Octobre 2016 - Janvier 2017</span>
            </div>
            <div class="training">
                <span class="training-title">BTS Négociation Relation Client en alternance</span>

                <span class="training-date"><br>2016</span>
            </div>
            <div class="training">
                <span class="training-title">BAC STI2D (Spécialité Système d'Informations et Numérique)</span>
                <span class="training-date"><br>Octobre 2016 - Janvier 2017</span>
            </div>
            <div class="training">
                <span class="training-title">BAFA (Brevet d'Aptitudes aux Fonctions d'Animateur)</span>
                <span class="training-date"><br> 2013 </span>
            </div>
        </div>
    </section>

    <!-- Formation -->

    <div class="background4" data-speed="35">
        <div class="filter">
        </div>
    </div>

    <!-- Contact -->

    <section class="contact" id="5">

        <div class="section-title">
            <h1 id="profile-title" class="title is-1">- CONTACTEZ <span id="profile-title2" class="title is-1">MOI
                    !</span> -</h1>
        </div>

        <div class="contact-box">
            <div class="contact-fade"></div>
            <div class="contact-form">
                <form action="mail.php" method="POST">
                    <div class="field">
                        <label class="label">Nom</label>
                        <div class="control">
                            <input id="lastname" class="input" type="text" placeholder="" name="lastname">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Prénom</label>
                        <div class="control">
                            <input id="firstname" class="input" type="text" placeholder="" name="firstname">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Objet</label>
                        <div class="control">
                            <input id="object" class="input" type="text" placeholder="" name="object">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Email</label>
                        <div class="control">
                            <input id="email" class="input" type="email" placeholder="" name="email">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Message</label>
                        <div class="control">
                            <textarea id="message" class="textarea" placeholder="" name="message"></textarea>
                        </div>
                    </div>
                    <button class="button is-link" type="submit" id="send_email">Envoyer</button>
                    <a href="tel:+33674815756"><img id=phone src="assets/img/phone.png" alt="telephone"> </a>
                </form>
                
            </div>
        </div>
    </section>

        <!-- Fin contact -->

        <!-- Footer-->

        

        <footer>
        
            <ul>
                <li>
                    <a href="https://www.linkedin.com/in/nicolashermosilla/" target="_blank">
                        <i class="fab fa-linkedin"></i>
                    </a>
                </li>
            

            
                <li>
                    <a href="https://www.instagram.com/nicoss0013/?hl=fr" target="_blank">
                        <img src="assets/img/instagram.png">
                    </a>
                </li>

                <li>
                    <a href="https://twitter.com/nicoss0013?lang=fr" target="_blank">
                        <img src="assets/img/twitter.png">
                    </a>
                </li>
            </ul>

            <a href="login.php" target="_blank">
                <i class="fas fa-user-cog button-admin"></i>
            </a>

        </footer>





        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/0.2.0/Chart.min.js'></script>

        <script src="assets/js/main.js"></script>
</body>
<!-- body -->

</html>