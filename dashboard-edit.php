<html>

<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css"
    integrity="sha384-Mmxa0mLqhmOeaE8vgOSbKacftZcsNYDjQzuCOm6D02luYSzBG8vpaOykv9lFQ51Y" crossorigin="anonymous">
<!-- /Font -->

  <!-- header -->
<head>

  <meta charset="utf-8" />
  <title>Admin</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/style/dashboard.css">

  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
    selector: '#mytextarea'
    });
</script>

</head>

   <!-- header -->
<body>
  <header>
    
    <h1><i class="fas fa-signal"></i>Dashboard</h1>
    <ul class="utilities">
      <br>
      <li class="users"><a href="#">Mon compte</a></li>
      <li class="logout warn"><a href="signout.php">Déconnexion</a></li>
    </ul>
  </header>
    
  <div class="dashboard-content">
    <nav>
      <ul class="main">
        <a href="login.php"><li class="dashboard"><i class="fas fa-tachometer-alt"></i>Statistique</li></a>
        <a href="dashboard-edit.php"><li class="edit"><i class="fas fa-pencil-alt"></i>Edit Website</li></a>
        <a href="#"><li class="mail"><i class="far fa-envelope"></i>Mail</li></a>
      </ul>
    </nav>
      
    <div class="content">

        <h1>Editer votre article</h1>
        <form method="post">
          <textarea id="mytextarea">COMMERCIAL</textarea>
          <button class="button is-link" type="submit" id="edit">Editer</button>
        </form>

    </div>

  </div>


</body>
</html>
  