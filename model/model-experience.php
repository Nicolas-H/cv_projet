<div class="job">
    <span class="job-title">
        <?php echo $experience["titre"]?>
    </span>
    <br>
    <span class="date-detail">
        <?php echo $experience["date"]?>, <?php echo $experience["employeur"]?>
    </span>
    <div class="job-detail">
        <?php echo $experience["description"]?>
    </div>
</div> 

