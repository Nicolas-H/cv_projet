var isChartLoaded = false;
// burger button 
let isVisible = false;
document.getElementById('burgerButton').addEventListener('click', function () {
    if (isVisible) {
        document.getElementsByTagName('nav')[0].classList.remove('open');
        document.getElementsByTagName('nav')[0].classList.add('close');
    } else {
        document.getElementsByTagName('nav')[0].classList.remove('close');
        document.getElementsByTagName('nav')[0].classList.add('open');
    }
    isVisible = !isVisible;
});


// Fermer menu mobile quand click

if ($(window).width() < 901) {

    $('nav a').on('click', function () {
        if (isVisible) {
            document.getElementsByTagName('nav')[0].classList.remove('open');
            document.getElementsByTagName('nav')[0].classList.add('close');
        } else {
            document.getElementsByTagName('nav')[0].classList.remove('close');
            document.getElementsByTagName('nav')[0].classList.add('open');
        }
        isVisible = !isVisible;
    });
}


// transition smooth navbar

$(document).ready(function () {
    $(window).on('scroll', function () {
        if (window.innerWidth > 900 && Math.round($(window).scrollTop()) > 305) {
            $('.header').addClass('scrolled');
        } else {
            $('.header').removeClass('scrolled');
        }
    });

});







// PARALAX

$(document).ready(function () {
    'use strict';





    // Paralax Banner 

    if ($(window).width() >= 901) {
        $('.banner').each(function () {
            var e = $(this);
            $(window).scroll(function () {
                var t = ($(window).scrollTop() / e.data("speed"));
                var n = "50% " + t + "px";
                e.css({ backgroundPosition: n })
            })
        });
    }




    // Transition Banner

    (function () {
        var header = $('.banner-content');
        var range = 200;
        $(window).on('scroll', function () {
            var scrollTop = $(this).scrollTop();
            var offset = 0; //header.offset().top;
            var height = header.outerHeight();
            offset = offset + height / 2;
            var calc = 1.8 - (scrollTop - offset + range) / range;
            header.css({ opacity: calc });
            if (calc > '1') {
                header.css({ opacity: 1 });
            } else if (calc < '0') {
                header.css({ opacity: 0 });
            }
        });
    }());
})


// Paralax Background
if ($(window).width() >= 901) {

    $('.background').each(function () {
        var e = $(this);
        $(window).scroll(function () {
            var t = ($(window).scrollTop() / e.data("speed"));
            var n = "50% " + t + "px";
            e.css({ backgroundPosition: n })
        })
    });


    // Paralax Background2
    $('.background2').each(function () {
        var e = $(this);
        $(window).scroll(function () {
            var t = ($(window).scrollTop() / e.data("speed"));
            var n = "50% " + t + "px";
            e.css({ backgroundPosition: n })
        })
    });
}




// Skill chart doughtnuts

let skills = [
    {
        value: 75,
        id: "html"
    },
    {
        value: 65,
        id: "css"
    },
    {
        value: 60,
        id: "ws"
    },
    {
        value: 85,
        id: "vb"
    },
    {
        value: 40,
        id: "python"
    },
    {
        value: 80,
        id: "windows"
    },
    {
        value: 90,
        id: "mac"
    },
    {
        value: 40,
        id: "linux"
    }






]
// Enabled charts effect when image appear on windows

$(document).ready(function () {
    $(window).on('scroll', function () {
        if (Math.round($(window).scrollTop()) > 1900 && !isChartLoaded) {
            isChartLoaded = true;
            buildAllCharts(skills);
        }
    });

});

function buildAllCharts(data) {
    data.forEach(skill => {
        createDonut(skill.value, skill.id);
    });
}

// Couleur des doughnut

function createDonut(value, htmlId) {
    let doughnut = document.getElementById(htmlId).getContext("2d");
    let data = [
        {
            value: value,
            color: "#74cfae"
        },
        {
            value: 100 - value,
            color: "#f2f2f2"
        },
    ]

    let finalDoughnut = new Chart(doughnut).Doughnut(data, {
        percentageInnerCutout: 80
    });
}




/*

// send mail with ajax

$('#send_email').click(function(e){
    e.preventDefault();
    var data = {
        email: $('#email').val(),
        name: $('#name').val(),
        firstname: $('#firstname').val(),
        message: $('#message').val()
    };
    // AJAX
    $.ajax({
        url: "mail.php",
        type: 'POST',
        data: data,
        success: function(data) {
            $('#js_alert_success').css({'display' : 'block'});
            setTimeout(function(){
                $('#js_alert_success').css({'display' : 'none'});
                $('#email').val("");
                $('#name').val("");
                $('firstname').val("");
                $('#message').val("")
            }, 3000);
        },
        error: function(data) {
            $('#js_alert_danger').css({'display' : 'block'});
            setTimeout(function(){
                $('#js_alert_danger').css({'display' : 'none'});
                $('#email').val("");
                $('#name').val("");
                $('firstname').val("");
                $('#message').val("")
            }, 3000);
        }
    });
});

*/