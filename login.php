<?php
  include('connect.php');
  $isDashboardVisible = false;
  $isAnError = false;

  // Test si l'utilisateur viens d'arriver sur la page ou si il a dejà envoyé le formulaire
  if(isset($_POST) && isset($_POST['login'])) {
    // Si on rentre ici c'est que le formulaire a été envoyé
    // recuperer les valeur du formulaire dans login.php
    $username = $_POST['login'];
    $password = $_POST['password'];

    // Appel de la fonction d'authentification
    $isDashboardVisible = connectUser($username, $password);
    $isAnError = !$isDashboardVisible;
  }
  
if(!isset($_SESSION["id"]) || empty($_SESSION["id"])) {
?>
<html>

<head>

  <meta charset="utf-8" />
  <title>Admin</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/style/login.css">

</head>

<body id="appform-page">
  <div id="appform-wrapper">
    <div class="appform-container clearfix">
      <div class="panel panel-small">
        <h2 class="panel-heading">Login</h2>

        <?php
          if($isAnError)
          {

            ?>

              <h4 class="error">Nom d'utilisateur ou mot de passe erroné</h4>

            <?php

          }

        ?>


        <form id="panel-form" action="#" method="POST">
          <div class="appform-group">
            <label>Utilisateur</label>
            <input type="text" name="login" required="" />
          </div>
          <div class="appform-group">
            <label>Mot de passe</label>
            <input type="password" name="password" required="" />
          </div>
          <input type="submit" class="appform-button" value="Connexion" />
        </form><br />
      </div>
    </div>
  </div>

  <!-- JS -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="assets/js/login.js"></script>

</body>

</html>

<?php 
}
else{
  include('dashboard.php');
}

?>