<html>

<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css" integrity="sha384-Mmxa0mLqhmOeaE8vgOSbKacftZcsNYDjQzuCOm6D02luYSzBG8vpaOykv9lFQ51Y" crossorigin="anonymous">
<!-- /Font -->

<!-- header -->

<head>

  <meta charset="utf-8" />
  <title>Admin</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/style/dashboard.css">
  <script>
    src = "dist/Chart.bundle.js"
  </script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


</head>

<?php
// Check si visiteur est unique
$visitor_ip = $_SERVER['REMOTE_ADDR'];
// Ajout d'un nouveau visiteur
//$req = $db->query("INSERT INTO visits(`ip_address`) VALUES('$visitor_ip')");
//$result= $req->fetch();
$req = $db->prepare("INSERT INTO visits(ip_address) VALUES(:ip_address)");
$req->execute(array(
  "ip_address" => $visitor_ip
));

$req = $db->query("SELECT count(id) nb_view FROM visits");
print_r($req)
$visits = $req->fetch();

$sql = "SELECT * FROM visits"; //WHERE ip_address = '$visitor_ip'";
//$req = $db->query("SELECT * FROM visits WHERE ip_address = '$visitor_ip'");
//$result= $req->fetch();
$datas = array();
$dates = array();

try {
  $stmt = $db->prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
  $stmt->execute();

  $index = 0;
  while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {

    $split = explode('-', $row[1]);

    $split[2] = preg_replace('/\s\d\d:\d\d:\d\d/', '', $split[2]);

    if ($index > 0 && $dates[$index - 1][0] == $split[0] && $dates[$index - 1][1] == $split[1] && $dates[$index - 1][2] == $split[2]) {
      $datas[$index - 1] = $datas[$index - 1] + 1;
    } else {
      array_push($datas, 1);
      array_push($dates, $split);
      $index++;
    }
  }

  print_r($datas);
} catch (PDOException $e) {
  echo 'An error occured';
  print_r($e);
}


?>

<!-- header -->

<body>
  <header>

    <h1><i class="fas fa-signal"></i>Dashboard</h1>
    <ul class="utilities">
      <br>
      <li class="users"><a href="#">Mon compte</a></li>
      <li class="logout warn"><a href="signout.php">Déconnexion</a></li>
    </ul>
  </header>

  <div class="dashboard-content">
    <nav>
      <ul class="main">
        <a href="#">
          <li class="dashboard"><i class="fas fa-tachometer-alt"></i>Statistique</li>
        </a>
        <a href="dashboard-edit.php">
          <li class="edit"><i class="fas fa-pencil-alt"></i>Edit Website</li>
        </a>
        <a href="#">
          <li class="mail"><i class="far fa-envelope"></i>Mail</li>
        </a>
      </ul>
    </nav>

    <div class="content">
      <div class="total-views">
        <h2>Nombre total de vues</h2>
        <span><?php echo $visits["nb_view"]; ?></span>
      </div>

      <canvas id="myChart" width="30" height="10"></canvas>
      <div></div>
    </div>
  </div>

  <script>
   
    var ctx = document.getElementById('myChart').getContext('2d');

    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'line',

      // The data for our dataset
      data: {
        labels: <?php 
                  echo '['; 
                  for($i = 0; $i < sizeof($dates); $i++) {
                    echo '\''.$dates[$i][2].'/'.$dates[$i][1].'/'.$dates[$i][0].'\'';
                    echo ',';
                  }
                  echo ']';
                ?>,
        datasets: [{
          label: 'Nombre de visites / mois',
          backgroundColor: '',
          borderColor: 'rgb(255, 99, 132)',
          data: <?php 
                  echo '['; 
                  for($i = 0; $i < sizeof($datas); $i++) {
                    echo $datas[$i];
                    echo ',';
                  }
                  echo ']';
                ?>
        }]
      },

      // Configuration options go here
      options: {}
    });
  </script>

</body>

</html>